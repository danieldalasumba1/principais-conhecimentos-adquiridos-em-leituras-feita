## Materiais granulares.

Desde os primórdios da humanidade o homem tem se apropriado dos materiais granulares para realizar diversas atividades, desde a era paleontóloga o homem já utilizava argila, produtos animais, cosméticos feito com tinta, gordura e etc. Atualmente os cientistas vêem desenvolver estudos sobre os materiais granulares, principalmente no campo de engenharia, e física,no nosso quotidiano nos deparamos com vários resultado desses estudos.

A física propõe principalmente nos estudos dos meios granulares secos, fato que atrai muitos cientistas devido a sua particularidade incomum. As interações preponderantes são as colisões inelásticas e fricção ,que são de culto alcance e não coeso, e os efeitos dos fluidos intersticiais nos meios granulares secos são insignificantes para a partícula dinâmica.

Mas então,o que são materiais granulares ?. Materiais granulares são conjuntos de partículas que possuem a principal característica a não coesão,com uma fronteira bem definida entre os elementos que o compõem ,cuja a gravidade e a força de repulsão possuem bastante protagonismo.

Um fator bastante interessante nos materiais granulares ,é que devido a propriedade macroscópica de sua partícula o ruído térmico não as movimenta ,e suas interações são dissipativo. As partículas ficam em repouso como um sólido,e flutuantes como um líquido.

A areia da praia é um exemplo de um material granular na condição úmida, e muitos experimentos foram feitos nesse meio granular, e muitas delas envolvendo propriedade mecânica de mídia granular úmida que são coesos em sua superfície, fator este que o diferencia do meio granular seco, outros exemplos de meio granular úmido é o deslizamento de terra induzidos pela chuva, produtos farmacêuticos, processamento de alimentos, mineração e indústrias de construção.

Os materiais granulares se comportam de maneira diferente dos sólidos, líquidos e gases, podendo então ser considerados como um outro estado da matéria, devido às suas propriedades e comportamentos tão particulares.

Duas características básicas definem esses materiais: a temperatura não tem influência sobre o sistema e a interação entre os grãos é dissipativo devido ao atrito estático e a colisões inelásticas entre eles. Por exemplo, uma pilha de areia mantém-se em equilíbrio se suas encostas tiverem um ângulo menor do que um certo valor. Se a pilha for inclinada acima de um ângulo crítico (por exemplo adicionando-se mais areia à pilha), alguns grãos começarão a se mover – a chamada avalanche .No entanto,este movimento,ou fluxo de areia,é complemente diferente do fluxo de um líquido, pois ele só existe na superfície da pilha de areia ,estando todo o resto dela em repouso. Um dos maiores efeitos que o líquido induz nos meio granular é a coesão entre grãos ,a umidade do ar pode causar uma pequena ponte de líquido em um ponto de contato que introduz coesão.

## Capilaridade


Denomina-se capilaridade a capacidade que as substâncias possuem de subirem e descerem em tubos capilares (tubos finos), ou seja a capacidade que elas possuem de se locomover em curto espaço de material poroso, como por exemplo em uma esponja, pano ou algodão, eles se locomovem até mesmo contra a força de gravidade.

Existem duas forças observáveis quando os líquidos são submetidos ao contato com uma superfície sólida, que são a coesão e adesão, tais forças são contrárias entre si.

A coesão mantém as moléculas dos líquidos ligados entre si, através da atração intermolecular,já a adesão está relacionada a atração das moléculas da água com a molécula do tubo sólido.
O fenômeno da capilaridade ocorre quando as moléculas da água colidem e aderem com as do tubo através da adesão, e afastam as demais através da coesão.

Quanto maior o diâmetro do tubo, menor é o número de moléculas de líquido que se aderem a parede em relação às que são arrastadas para cima por coesão. Sendo assim, sabendo o diâmetro do tubo pode se calcular a altura que o líquido atinge em seu interior, através da força da capilaridade.

Quanto mais fino for o tubo maior a aderência e quanto mais quente for o líquido menos viscoso será o líquido, ou seja, o fator viscosidade depende da temperatura.

Imaginemos a seguinte situação: Um recipiente com água e pegamos um outro objeto como por exemplo palha, se eu mergulhar uma parte dele na água, então como as moléculas de água são atraídas pela parede desse recipiente menor que estará dentro dele na água, como as moléculas de água são atraídas pela parede do recipiente menor que estará dentro,a água não permanecerá no mesmo nível,ou seja o nível de água vai subir até um certo nível maior do que a do recipiente maior,e se o recipiente for mais fino maior vai ser o nível de água que vai subir pelo recipiente. Esse fenômeno é o que chamamos de capilaridade.

## Tensão superficial 

É uma propriedade que acontece na superfície de líquidos,como água,formando uma fina película, essa propriedade ocorre devido a atração de suas moléculas em se manter unidas por intermédio da força de interação, essa atração é causada pelas forças de coesão entre moléculas semelhantes,às moléculas interagem próximas uma das outras no centro do líquido.

Na superfície do líquido há uma divisão de forças não tão atrativas quanto no centro,as moléculas interagem próximas uma das outras e as moléculas abaixo puxam as de cima para superfície,porém, em cima do líquido não tem moléculas para puxar,há uma dispersão de forças não muito igualitária consequentemente essas moléculas da superfície ficam mais coesas mais próximas umas das outras formando assim o fenômeno que chamamos de tensão superficial,o fio de moléculas coesas que se encontram na superfície.

Podemos notar por exemplo, uma lâmina por cima de um copo com água,a lâmina flutua,devido a tensão superficial.   
Como um inseto consegue andar na superfície da água se muitas das vezes o inseto acaba sendo mais denso que a água?. 
Quando á água em estado líquido,ocupa um recipiente,podemos perceber a separação que há entre o líquido e o ambiente. Isso ocorre porque a interação entre as moléculas de água na superfície é diferente das interações no interior do líquido.

Na superfície uma molécula de água interage com as moléculas das laterais e abaixo dela. No interior uma molécula é rodeada por outras moléculas e há interação em todas as direções por meio das ligações de hidrogênio. Motivo este que permite os insetos caminhem pela água.

A tensão superficial é responsavél pela integridade fisíca dos liquidos dando a eles uma mecanica natural,distanciando-os de problemas relacionados ao contraste entre superficies.

### O que acontece quando esprememos um pano molhado no espaço?.

Quando esprememos um pano molhado no espaço ,observamos que as moléculas da água se mantém unida,grudando no pano,esse fenômeno se dá devido a tensão superficial.

Bibliografia:

 [https://gitlab.com/danieldalasumba1/advances-in-physics/-/blob/master/Materiais_granulares_%C3%BAmidos.md_1.mdibliografia](https://gitlab.com/danieldalasumba1/advances-in-physics/-/blob/master/Materiais_granulares_úmidos.md_1.mdibliografia):

chrome-extension://ohfgljdgelakfkefopgklcohadegdpjf/http://www.www1.fisica.org.br/fne/phocadownload/Vol02-Num1/granulares1.pdf

chrome-extension://ohfgljdgelakfkefopgklcohadegdpjf/http://www2.dbd.puc-rio.br/pergamum/tesesabertas/0510944_09_cap_01.pdf 

https://educacao.uol.com.br/disciplinas/quimica/capilaridade-a-passagem-natural-do-liquido-por-um-tubo-muito-fino.htm
